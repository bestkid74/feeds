<?php
header('Access-Control-Allow-Origin: *');

require __DIR__ . '/../vendor/autoload.php';

require_once __DIR__ . '/../vendor/j7mbo/twitter-api-php/TwitterAPIExchange.php';

$dotenv = new \Dotenv\Dotenv(dirname(__DIR__));
$dotenv->load();

/**
 * simple routing by url params
 */
$url = $_SERVER["REQUEST_URI"];
$query_str = parse_url($url, PHP_URL_QUERY);
parse_str($query_str, $query_params);

/**
 * get Instagram
 */
if(isset($query_params['target']) && $query_params['target'] === 'instagram') {
    define('TOKEN', getenv('TOKEN'));

    $user_id = 'self';
    $instagram_cnct = curl_init();
    curl_setopt( $instagram_cnct, CURLOPT_URL, "https://api.instagram.com/v1/users/" . $user_id
        . "/media/recent?access_token=" . TOKEN );
    curl_setopt( $instagram_cnct, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt( $instagram_cnct, CURLOPT_TIMEOUT, 15 );
    $media = curl_exec( $instagram_cnct );
    curl_close( $instagram_cnct );

    echo $media;
}

/**
 * get Twitter
 */
if(isset($query_params['target']) && $query_params['target'] === 'twitter') {
    define('CONSUMER_KEY', getenv('CONSUMER_KEY'));
    define('CONSUMER_SECRET', getenv('CONSUMER_SECRET'));
    define('ACCESS_TOKEN',getenv('ACCESS_TOKEN'));
    define('ACCESS_SECRET', getenv('ACCESS_SECRET'));
    define('TARGET', getenv('TARGET'));
    define('COUNT', getenv('COUNT'));

    $settings = array(
        'oauth_access_token' => ACCESS_TOKEN,
        'oauth_access_token_secret' => ACCESS_SECRET,
        'consumer_key' => CONSUMER_KEY,
        'consumer_secret' => CONSUMER_SECRET
    );

    $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
    $getfield = '?screen_name=' . TARGET . '&count=' . COUNT;
    $requestMethod = 'GET';
    $twitter = new TwitterAPIExchange($settings);
    echo json_encode($twitter->setGetfield($getfield)
        ->buildOauth($url, $requestMethod)
        ->performRequest());
}
