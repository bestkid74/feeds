import React, { Component } from 'react';
//import axios from 'axios';

import { List, Spin, Button } from 'antd';

class TwitterFeed extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            loading: true,
            loadingMore: false,
            showLoadingMore: true
        }

        this.onLoadMore = this.onLoadMore.bind(this)
    }

    componentWillMount() {
        this.getData();
    }

    getData() {
        // axios.get('/feeds.php?target=twitter')
        //     .then((response) => {
        //         this.setState({
        //             data: response.data,
        //             loading: false
        //         });
        //     })
        //     .catch(function (error) {
        //         // handle error
        //         console.log(error);
        //     });
    }

    onLoadMore() {
        this.setState({
            loadingMore: true,
        });

        this.getData();
    }

    render() {
        const { loading, loadingMore, showLoadingMore, data } = this.state;
        const loadMore = showLoadingMore ? (
            <div style={{ textAlign: 'center', marginTop: 12, height: 32, lineHeight: '32px' }}>
                {loadingMore && <Spin />}
                {!loadingMore && <Button onClick={this.onLoadMore}>Обновить</Button>}
            </div>
        ) : null;

        return (
            <div>
                <List loading={loading}
                      loadMore={loadMore}
                      dataSource={data}
                      renderItem={item => (
                          <List.Item>
                              {item.text}
                          </List.Item>
                      )}
                />
            </div>
        );
    }
}

export default TwitterFeed;