import React, { Component } from 'react';
import axios from 'axios';

import { List, Card, Spin, Button } from 'antd';

class InstagramFeed extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            loading: true,
            loadingMore: false,
            showLoadingMore: true
        }

        this.onLoadMore = this.onLoadMore.bind(this)
    }

    componentWillMount() {
        this.getData();
    }

    getData() {
        axios.get('http://feeds.local/feeds.php?target=instagram')
            .then((response) => {
                this.setState({
                    data: response.data.data,
                    loading: false
                });
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            });
    }

    onLoadMore() {
        this.setState({
            loadingMore: true,
        });

        this.getData();
    }

    render() {
        const { loading, loadingMore, showLoadingMore, data } = this.state;
        const loadMore = showLoadingMore ? (
            <div style={{ textAlign: 'center', marginTop: 12, height: 32, lineHeight: '32px' }}>
                {loadingMore && <Spin />}
                {!loadingMore && <Button onClick={this.onLoadMore}>Обновить</Button>}
            </div>
        ) : null;

        return (
            <div>
                <List grid={{ gutter: 10, xs: 1, sm: 2, md: 4, lg: 4, xl: 4}}
                      loading={loading}
                      loadMore={loadMore}
                      dataSource={data}
                      renderItem={item => (
                          <List.Item>
                              <Card hoverable
                                    style={{ width: '100%', maxWidth: 240, margin: '0 auto' }}
                                    cover={<img alt="example" src={item['images']['standard_resolution']['url']} />}
                              >
                                  Комментариев: {item['comments']['count']}
                              </Card>
                          </List.Item>
                      )}
                />

            </div>
        );
    }
}

export default InstagramFeed;