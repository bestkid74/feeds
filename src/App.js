import React, { Component } from 'react';
import './App.css';

import { Layout, Menu, Icon } from 'antd';
import 'antd/dist/antd.css';

import InstagramFeed from './components/InstagramFeed';
import TwitterFeed from './components/TwitterFeed';

const { Header, Content, Footer } = Layout;

class App extends Component {
    constructor(props){
        super(props);

        this.state = {
            current: 'instagram'
        };

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {
        this.setState({
            current: e.key
        });
    }

  render() {
    return (
      <div className="App">
          <Layout className="layout">
              <Header>
                  <div className="logo" />
                  <Menu
                      theme="dark"
                      mode="horizontal"
                      defaultSelectedKeys={['instagram']}
                      onClick={this.handleClick}
                      selectedKeys={[this.state.current]}
                      style={{ lineHeight: '64px' }}
                  >
                      <Menu.Item key="instagram">
                          <Icon type="instagram" />
                          Instagram
                      </Menu.Item>
                      <Menu.Item key="twitter">
                          <Icon type="twitter" />
                          Twitter
                      </Menu.Item>
                      <Menu.Item key="facebook">
                          <Icon type="facebook" />
                          Facebook
                      </Menu.Item>
                  </Menu>
              </Header>
              <Content style={{ padding: '0 50px' }}>
                  <h2 style={{ margin: '16px 0', textTransform: 'capitalize' }}>{ this.state.current }</h2>
                  <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
                      {this.state.current === 'instagram' && <InstagramFeed />}
                      {this.state.current === 'twitter' && <TwitterFeed />}
                      {this.state.current === 'facebook' && <div>Coming soon...</div>}
                  </div>
              </Content>
              <Footer style={{ textAlign: 'center' }}>
                  Ant Design ©2018 Created by Ant UED
              </Footer>
          </Layout>
      </div>
    );
  }
}

export default App;
